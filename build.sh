#!/bin/bash

set -eu

TOPDIR=$(cd $(dirname $) && pwd)
cd $TOPDIR
. common.sh

mkdir -p $BUILDARTIFACTSDIR $BUILDMODDIR
git clone -b $FIRMWAREBRANCH --depth 1 $FIRMWAREURL $BUILDFIRMWAREDIR
mkdir $BUILDMODDIR/lib
tar -C $BUILDFIRMWAREDIR -cf - modules | tar -C $BUILDMODDIR/lib -xf -
tar -C $BUILDMODDIR -czf $MODTGZ .
tar -C $BUILDFIRMWAREDIR/boot -czf $BOOTTGZ .
tar -C $BUILDFIRMWAREDIR/hardfp -czf $VCTGZ opt/vc
